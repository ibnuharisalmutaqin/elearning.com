<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>elearning.com</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
        <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css" />
        <!-- Third party plugin CSS-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ url('front/css/styles.css') }}" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Masthead-->
        <header class="masthead">
            <div class="container h-100">
                <div class="row h-100 align-items-center justify-content-center text-center">
                    <div class="col-lg-10 align-self-end">
                        <h1 class="text-uppercase text-white font-weight-bold">Your Favorite Learning Source!</h1>
                        <hr class="divider my-4" />
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <p class="text-white-75 font-weight-light mb-5">elearning.com</p>
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="#services">Find Out More</a>
                    </div>
                </div>
            </div>
        </header>

        <!-- Services-->
        <section class="page-section">
            <div class="container" id="services">
                <h2 class="text-center mt-0">The Features</h2>
                <hr class="divider my-4" />
                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <i class="fab fa-4x fa-rocketchat text-primary mb-4"></i>
                            <h3 class="h4 mb-2">Grup Chating</h3>
                            <p class="text-muted mb-0">we provide group chat features to facilitate discussion</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <i class="fas fa-4x fa-paperclip text-primary mb-4"></i>
                            <h3 class="h4 mb-2">Attachment File</h3>
                            <p class="text-muted mb-0">you can send documents in the form of images, music and other documents</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <i class="fas fa-4x fa-video text-primary mb-4"></i>
                            <h3 class="h4 mb-2">Video Publish</h3>
                            <p class="text-muted mb-0">you can use this feature to display video material</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="mt-5">
                            <i class="fas fa-4x fa-user-edit text-primary mb-4"></i>
                            <h3 class="h4 mb-2">Quiz and Test</h3>
                            <p class="text-muted mb-0">you can make quizzes or tests on the material that you are providing</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <!-- Portfolio-->
        <section id="portfolio">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="{{ url('front/img/layout_and_beautiful.jpg') }}">
                            <img class="img-fluid" src="{{ url('front/img/layout_and_beautiful.jpg') }}" alt="">
                            <div class="portfolio-box-caption">
                                <div class="project-name">responsive layout and beautiful</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="{{ url('front/img/easy_to_use.jpg') }}"
                            ><img class="img-fluid" src="{{ url('front/img/easy_to_use.jpg') }}" alt="">
                            <div class="portfolio-box-caption">
                                <div class="project-name">powerful and easy to use</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="{{ url('front/img/easy_discussion.jpg') }}"
                            ><img class="img-fluid" src="{{ url('front/img/easy_discussion.jpg') }}" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name">easy discussion in one group</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="{{ url('front/img/documents_can_be_sent.jpg') }}"
                            ><img class="img-fluid" src="{{ url('front/img/documents_can_be_sent.jpg') }}" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name">many types of documents can be sent</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="{{ url('front/img/see_video_material.jpg') }}">
                            <img class="img-fluid" src="{{ url('front/img/see_video_material.jpg') }}" alt="" />
                            <div class="portfolio-box-caption">
                                <div class="project-name">easier by learning to see video material</div>
                            </div></a
                        >
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="{{ url('front/img/give_a_quiz.jpg') }}"
                            ><img class="img-fluid" src="{{ url('front/img/give_a_quiz.jpg') }}" alt="" />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-name">give a quiz to complete the material presented</div>
                            </div></a
                        >
                    </div>
                </div>
            </div>
        </section>

        <!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2 class="mt-0">We've got what you need!</h2>
                        <hr class="divider my-4">
                        <p class="text-muted mb-5">have everything you need to create a powerful and effective learning style. you can get it at <strong>elearning.com</strong></p>
                    </div>
                </div>
                <div class="row justify-content-center mb-5">
                    <div class="col-lg-8 text-center">
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="{{ route('login') }}">Get Started!</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                        <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                        <div>0123-4567-8910</div>
                    </div>
                    <div class="col-lg-4 mr-auto text-center">
                        <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
                        <div>contact@elearning.com</div>
                    </div>
                </div>
                
            </div>
        </section>
        <!-- Footer-->
        <footer class="bg-light py-5">
            <div class="container"><div class="small text-center text-muted">Copyright © 2020 - elearning.com</div></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ url('front/js/scripts.js') }}"></script>
    </body>
</html>